# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: openapiadmin/openapiadmin.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()


from common import common_pb2 as common_dot_common__pb2


DESCRIPTOR = _descriptor.FileDescriptor(
  name='openapiadmin/openapiadmin.proto',
  package='openapiadmin',
  syntax='proto3',
  serialized_options=_b('Z*bitbucket.org/acbapis/acbapis/openapiadmin'),
  serialized_pb=_b('\n\x1fopenapiadmin/openapiadmin.proto\x12\x0copenapiadmin\x1a\x13\x63ommon/common.proto\"\x93\x04\n\x11\x46lushCacheRequest\x12\x1a\n\x12roundDenominations\x18\x01 \x01(\x08\x12\x0f\n\x07matches\x18\x02 \x01(\x08\x12\x11\n\tstandings\x18\x03 \x01(\x08\x12\r\n\x05weeks\x18\x04 \x01(\x08\x12\x11\n\tschedules\x18\x05 \x01(\x08\x12\x17\n\x0fstatsCollection\x18\x06 \x01(\x08\x12\x0c\n\x04top5\x18\x07 \x01(\x08\x12\x0f\n\x07leaders\x18\x08 \x01(\x08\x12\x0f\n\x07\x61llTime\x18\t \x01(\x08\x12\x0f\n\x07records\x18\n \x01(\x08\x12\r\n\x05highs\x18\x0b \x01(\x08\x12\x13\n\x0b\x62\x61\x63kgrounds\x18\x0c \x01(\x08\x12\x12\n\nprecedents\x18\r \x01(\x08\x12\x14\n\x0c\x63ountryNames\x18\x0e \x01(\x08\x12\x13\n\x0b\x63uadroHonor\x18\x0f \x01(\x08\x12\x15\n\rteamsRankings\x18\x10 \x01(\x08\x12\x13\n\x0b\x63lubRanking\x18\x11 \x01(\x08\x12\x12\n\nteamSeason\x18\x12 \x01(\x08\x12\x14\n\x0c\x63oachSeasons\x18\x13 \x01(\x08\x12\x15\n\rplayerMatches\x18\x14 \x01(\x08\x12\x15\n\rplayerSeasons\x18\x15 \x01(\x08\x12\x13\n\x0bplayerHighs\x18\x16 \x01(\x08\x12\x18\n\x10teamsRankingList\x18\x17 \x01(\x08\x12\x1a\n\x12playersRankingList\x18\x18 \x01(\x08\x12\x10\n\x08rankings\x18\x19 \x01(\x08\x32\\\n\x13OpenApiAdminService\x12\x45\n\nFlushCache\x12\x1f.openapiadmin.FlushCacheRequest\x1a\x14.common.EmptyMessage\"\x00\x42,Z*bitbucket.org/acbapis/acbapis/openapiadminb\x06proto3')
  ,
  dependencies=[common_dot_common__pb2.DESCRIPTOR,])




_FLUSHCACHEREQUEST = _descriptor.Descriptor(
  name='FlushCacheRequest',
  full_name='openapiadmin.FlushCacheRequest',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='roundDenominations', full_name='openapiadmin.FlushCacheRequest.roundDenominations', index=0,
      number=1, type=8, cpp_type=7, label=1,
      has_default_value=False, default_value=False,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='matches', full_name='openapiadmin.FlushCacheRequest.matches', index=1,
      number=2, type=8, cpp_type=7, label=1,
      has_default_value=False, default_value=False,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='standings', full_name='openapiadmin.FlushCacheRequest.standings', index=2,
      number=3, type=8, cpp_type=7, label=1,
      has_default_value=False, default_value=False,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='weeks', full_name='openapiadmin.FlushCacheRequest.weeks', index=3,
      number=4, type=8, cpp_type=7, label=1,
      has_default_value=False, default_value=False,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='schedules', full_name='openapiadmin.FlushCacheRequest.schedules', index=4,
      number=5, type=8, cpp_type=7, label=1,
      has_default_value=False, default_value=False,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='statsCollection', full_name='openapiadmin.FlushCacheRequest.statsCollection', index=5,
      number=6, type=8, cpp_type=7, label=1,
      has_default_value=False, default_value=False,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='top5', full_name='openapiadmin.FlushCacheRequest.top5', index=6,
      number=7, type=8, cpp_type=7, label=1,
      has_default_value=False, default_value=False,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='leaders', full_name='openapiadmin.FlushCacheRequest.leaders', index=7,
      number=8, type=8, cpp_type=7, label=1,
      has_default_value=False, default_value=False,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='allTime', full_name='openapiadmin.FlushCacheRequest.allTime', index=8,
      number=9, type=8, cpp_type=7, label=1,
      has_default_value=False, default_value=False,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='records', full_name='openapiadmin.FlushCacheRequest.records', index=9,
      number=10, type=8, cpp_type=7, label=1,
      has_default_value=False, default_value=False,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='highs', full_name='openapiadmin.FlushCacheRequest.highs', index=10,
      number=11, type=8, cpp_type=7, label=1,
      has_default_value=False, default_value=False,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='backgrounds', full_name='openapiadmin.FlushCacheRequest.backgrounds', index=11,
      number=12, type=8, cpp_type=7, label=1,
      has_default_value=False, default_value=False,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='precedents', full_name='openapiadmin.FlushCacheRequest.precedents', index=12,
      number=13, type=8, cpp_type=7, label=1,
      has_default_value=False, default_value=False,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='countryNames', full_name='openapiadmin.FlushCacheRequest.countryNames', index=13,
      number=14, type=8, cpp_type=7, label=1,
      has_default_value=False, default_value=False,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='cuadroHonor', full_name='openapiadmin.FlushCacheRequest.cuadroHonor', index=14,
      number=15, type=8, cpp_type=7, label=1,
      has_default_value=False, default_value=False,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='teamsRankings', full_name='openapiadmin.FlushCacheRequest.teamsRankings', index=15,
      number=16, type=8, cpp_type=7, label=1,
      has_default_value=False, default_value=False,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='clubRanking', full_name='openapiadmin.FlushCacheRequest.clubRanking', index=16,
      number=17, type=8, cpp_type=7, label=1,
      has_default_value=False, default_value=False,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='teamSeason', full_name='openapiadmin.FlushCacheRequest.teamSeason', index=17,
      number=18, type=8, cpp_type=7, label=1,
      has_default_value=False, default_value=False,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='coachSeasons', full_name='openapiadmin.FlushCacheRequest.coachSeasons', index=18,
      number=19, type=8, cpp_type=7, label=1,
      has_default_value=False, default_value=False,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='playerMatches', full_name='openapiadmin.FlushCacheRequest.playerMatches', index=19,
      number=20, type=8, cpp_type=7, label=1,
      has_default_value=False, default_value=False,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='playerSeasons', full_name='openapiadmin.FlushCacheRequest.playerSeasons', index=20,
      number=21, type=8, cpp_type=7, label=1,
      has_default_value=False, default_value=False,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='playerHighs', full_name='openapiadmin.FlushCacheRequest.playerHighs', index=21,
      number=22, type=8, cpp_type=7, label=1,
      has_default_value=False, default_value=False,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='teamsRankingList', full_name='openapiadmin.FlushCacheRequest.teamsRankingList', index=22,
      number=23, type=8, cpp_type=7, label=1,
      has_default_value=False, default_value=False,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='playersRankingList', full_name='openapiadmin.FlushCacheRequest.playersRankingList', index=23,
      number=24, type=8, cpp_type=7, label=1,
      has_default_value=False, default_value=False,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='rankings', full_name='openapiadmin.FlushCacheRequest.rankings', index=24,
      number=25, type=8, cpp_type=7, label=1,
      has_default_value=False, default_value=False,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=71,
  serialized_end=602,
)

DESCRIPTOR.message_types_by_name['FlushCacheRequest'] = _FLUSHCACHEREQUEST
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

FlushCacheRequest = _reflection.GeneratedProtocolMessageType('FlushCacheRequest', (_message.Message,), dict(
  DESCRIPTOR = _FLUSHCACHEREQUEST,
  __module__ = 'openapiadmin.openapiadmin_pb2'
  # @@protoc_insertion_point(class_scope:openapiadmin.FlushCacheRequest)
  ))
_sym_db.RegisterMessage(FlushCacheRequest)


DESCRIPTOR._options = None

_OPENAPIADMINSERVICE = _descriptor.ServiceDescriptor(
  name='OpenApiAdminService',
  full_name='openapiadmin.OpenApiAdminService',
  file=DESCRIPTOR,
  index=0,
  serialized_options=None,
  serialized_start=604,
  serialized_end=696,
  methods=[
  _descriptor.MethodDescriptor(
    name='FlushCache',
    full_name='openapiadmin.OpenApiAdminService.FlushCache',
    index=0,
    containing_service=None,
    input_type=_FLUSHCACHEREQUEST,
    output_type=common_dot_common__pb2._EMPTYMESSAGE,
    serialized_options=None,
  ),
])
_sym_db.RegisterServiceDescriptor(_OPENAPIADMINSERVICE)

DESCRIPTOR.services_by_name['OpenApiAdminService'] = _OPENAPIADMINSERVICE

# @@protoc_insertion_point(module_scope)
