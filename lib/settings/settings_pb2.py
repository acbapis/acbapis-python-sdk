# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: settings/settings.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()


from google.api import annotations_pb2 as google_dot_api_dot_annotations__pb2


DESCRIPTOR = _descriptor.FileDescriptor(
  name='settings/settings.proto',
  package='settings',
  syntax='proto3',
  serialized_options=_b('Z&bitbucket.org/acbapis/acbapis/settings'),
  serialized_pb=_b('\n\x17settings/settings.proto\x12\x08settings\x1a\x1cgoogle/api/annotations.proto\"\x16\n\x06\x42ucket\x12\x0c\n\x04path\x18\x01 \x01(\t\"I\n\x07Setting\x12 \n\x06\x62ucket\x18\x01 \x01(\x0b\x32\x10.settings.Bucket\x12\x1c\n\x02kv\x18\x02 \x01(\x0b\x32\x10.settings.KeyVal\"?\n\x0cSettingArray\x12 \n\x05items\x18\x01 \x03(\x0b\x32\x11.settings.Setting\x12\r\n\x05total\x18\x02 \x01(\x05\"$\n\x06KeyVal\x12\x0b\n\x03key\x18\x01 \x01(\t\x12\r\n\x05value\x18\x02 \x01(\x0c\"=\n\x0bKeyValArray\x12\x1f\n\x05items\x18\x01 \x03(\x0b\x32\x10.settings.KeyVal\x12\r\n\x05total\x18\x02 \x01(\x05\")\n\x08\x45rrorMsg\x12\x0c\n\x04\x63ode\x18\x01 \x01(\x05\x12\x0f\n\x07message\x18\x02 \x01(\t2\x99\x04\n\x0eSettingService\x12N\n\rCreateSetting\x12\x11.settings.Setting\x1a\x11.settings.Setting\"\x17\x82\xd3\xe4\x93\x02\x11\"\x0c/v1/settings:\x01*\x12j\n\rUpdateSetting\x12\x11.settings.Setting\x1a\x11.settings.Setting\"3\x82\xd3\xe4\x93\x02-\x1a\x0c/v1/settings:\x01*Z\x1a\x32\x15/v1/settings/{kv.key}:\x01*\x12Q\n\nGetSetting\x12\x11.settings.Setting\x1a\x11.settings.Setting\"\x1d\x82\xd3\xe4\x93\x02\x17\x12\x15/v1/settings/{kv.key}\x12N\n\x0cListSettings\x12\x10.settings.Bucket\x1a\x16.settings.SettingArray\"\x14\x82\xd3\xe4\x93\x02\x0e\x12\x0c/v1/settings\x12U\n\rDeleteSetting\x12\x11.settings.Setting\x1a\x12.settings.ErrorMsg\"\x1d\x82\xd3\xe4\x93\x02\x17*\x15/v1/settings/{kv.key}\x12Q\n\x0c\x44\x65leteBucket\x12\x10.settings.Bucket\x1a\x12.settings.ErrorMsg\"\x1b\x82\xd3\xe4\x93\x02\x15*\x13/v1/settings/{path}B(Z&bitbucket.org/acbapis/acbapis/settingsb\x06proto3')
  ,
  dependencies=[google_dot_api_dot_annotations__pb2.DESCRIPTOR,])




_BUCKET = _descriptor.Descriptor(
  name='Bucket',
  full_name='settings.Bucket',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='path', full_name='settings.Bucket.path', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=67,
  serialized_end=89,
)


_SETTING = _descriptor.Descriptor(
  name='Setting',
  full_name='settings.Setting',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='bucket', full_name='settings.Setting.bucket', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='kv', full_name='settings.Setting.kv', index=1,
      number=2, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=91,
  serialized_end=164,
)


_SETTINGARRAY = _descriptor.Descriptor(
  name='SettingArray',
  full_name='settings.SettingArray',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='items', full_name='settings.SettingArray.items', index=0,
      number=1, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='total', full_name='settings.SettingArray.total', index=1,
      number=2, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=166,
  serialized_end=229,
)


_KEYVAL = _descriptor.Descriptor(
  name='KeyVal',
  full_name='settings.KeyVal',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='key', full_name='settings.KeyVal.key', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='value', full_name='settings.KeyVal.value', index=1,
      number=2, type=12, cpp_type=9, label=1,
      has_default_value=False, default_value=_b(""),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=231,
  serialized_end=267,
)


_KEYVALARRAY = _descriptor.Descriptor(
  name='KeyValArray',
  full_name='settings.KeyValArray',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='items', full_name='settings.KeyValArray.items', index=0,
      number=1, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='total', full_name='settings.KeyValArray.total', index=1,
      number=2, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=269,
  serialized_end=330,
)


_ERRORMSG = _descriptor.Descriptor(
  name='ErrorMsg',
  full_name='settings.ErrorMsg',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='code', full_name='settings.ErrorMsg.code', index=0,
      number=1, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='message', full_name='settings.ErrorMsg.message', index=1,
      number=2, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=332,
  serialized_end=373,
)

_SETTING.fields_by_name['bucket'].message_type = _BUCKET
_SETTING.fields_by_name['kv'].message_type = _KEYVAL
_SETTINGARRAY.fields_by_name['items'].message_type = _SETTING
_KEYVALARRAY.fields_by_name['items'].message_type = _KEYVAL
DESCRIPTOR.message_types_by_name['Bucket'] = _BUCKET
DESCRIPTOR.message_types_by_name['Setting'] = _SETTING
DESCRIPTOR.message_types_by_name['SettingArray'] = _SETTINGARRAY
DESCRIPTOR.message_types_by_name['KeyVal'] = _KEYVAL
DESCRIPTOR.message_types_by_name['KeyValArray'] = _KEYVALARRAY
DESCRIPTOR.message_types_by_name['ErrorMsg'] = _ERRORMSG
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

Bucket = _reflection.GeneratedProtocolMessageType('Bucket', (_message.Message,), dict(
  DESCRIPTOR = _BUCKET,
  __module__ = 'settings.settings_pb2'
  # @@protoc_insertion_point(class_scope:settings.Bucket)
  ))
_sym_db.RegisterMessage(Bucket)

Setting = _reflection.GeneratedProtocolMessageType('Setting', (_message.Message,), dict(
  DESCRIPTOR = _SETTING,
  __module__ = 'settings.settings_pb2'
  # @@protoc_insertion_point(class_scope:settings.Setting)
  ))
_sym_db.RegisterMessage(Setting)

SettingArray = _reflection.GeneratedProtocolMessageType('SettingArray', (_message.Message,), dict(
  DESCRIPTOR = _SETTINGARRAY,
  __module__ = 'settings.settings_pb2'
  # @@protoc_insertion_point(class_scope:settings.SettingArray)
  ))
_sym_db.RegisterMessage(SettingArray)

KeyVal = _reflection.GeneratedProtocolMessageType('KeyVal', (_message.Message,), dict(
  DESCRIPTOR = _KEYVAL,
  __module__ = 'settings.settings_pb2'
  # @@protoc_insertion_point(class_scope:settings.KeyVal)
  ))
_sym_db.RegisterMessage(KeyVal)

KeyValArray = _reflection.GeneratedProtocolMessageType('KeyValArray', (_message.Message,), dict(
  DESCRIPTOR = _KEYVALARRAY,
  __module__ = 'settings.settings_pb2'
  # @@protoc_insertion_point(class_scope:settings.KeyValArray)
  ))
_sym_db.RegisterMessage(KeyValArray)

ErrorMsg = _reflection.GeneratedProtocolMessageType('ErrorMsg', (_message.Message,), dict(
  DESCRIPTOR = _ERRORMSG,
  __module__ = 'settings.settings_pb2'
  # @@protoc_insertion_point(class_scope:settings.ErrorMsg)
  ))
_sym_db.RegisterMessage(ErrorMsg)


DESCRIPTOR._options = None

_SETTINGSERVICE = _descriptor.ServiceDescriptor(
  name='SettingService',
  full_name='settings.SettingService',
  file=DESCRIPTOR,
  index=0,
  serialized_options=None,
  serialized_start=376,
  serialized_end=913,
  methods=[
  _descriptor.MethodDescriptor(
    name='CreateSetting',
    full_name='settings.SettingService.CreateSetting',
    index=0,
    containing_service=None,
    input_type=_SETTING,
    output_type=_SETTING,
    serialized_options=_b('\202\323\344\223\002\021\"\014/v1/settings:\001*'),
  ),
  _descriptor.MethodDescriptor(
    name='UpdateSetting',
    full_name='settings.SettingService.UpdateSetting',
    index=1,
    containing_service=None,
    input_type=_SETTING,
    output_type=_SETTING,
    serialized_options=_b('\202\323\344\223\002-\032\014/v1/settings:\001*Z\0322\025/v1/settings/{kv.key}:\001*'),
  ),
  _descriptor.MethodDescriptor(
    name='GetSetting',
    full_name='settings.SettingService.GetSetting',
    index=2,
    containing_service=None,
    input_type=_SETTING,
    output_type=_SETTING,
    serialized_options=_b('\202\323\344\223\002\027\022\025/v1/settings/{kv.key}'),
  ),
  _descriptor.MethodDescriptor(
    name='ListSettings',
    full_name='settings.SettingService.ListSettings',
    index=3,
    containing_service=None,
    input_type=_BUCKET,
    output_type=_SETTINGARRAY,
    serialized_options=_b('\202\323\344\223\002\016\022\014/v1/settings'),
  ),
  _descriptor.MethodDescriptor(
    name='DeleteSetting',
    full_name='settings.SettingService.DeleteSetting',
    index=4,
    containing_service=None,
    input_type=_SETTING,
    output_type=_ERRORMSG,
    serialized_options=_b('\202\323\344\223\002\027*\025/v1/settings/{kv.key}'),
  ),
  _descriptor.MethodDescriptor(
    name='DeleteBucket',
    full_name='settings.SettingService.DeleteBucket',
    index=5,
    containing_service=None,
    input_type=_BUCKET,
    output_type=_ERRORMSG,
    serialized_options=_b('\202\323\344\223\002\025*\023/v1/settings/{path}'),
  ),
])
_sym_db.RegisterServiceDescriptor(_SETTINGSERVICE)

DESCRIPTOR.services_by_name['SettingService'] = _SETTINGSERVICE

# @@protoc_insertion_point(module_scope)
