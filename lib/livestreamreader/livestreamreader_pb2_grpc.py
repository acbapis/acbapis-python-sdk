# Generated by the gRPC Python protocol compiler plugin. DO NOT EDIT!
import grpc

from common import common_pb2 as common_dot_common__pb2
from livestreamreader import livestreamreader_pb2 as livestreamreader_dot_livestreamreader__pb2


class MatchReadServiceStub(object):
  # missing associated documentation comment in .proto file
  pass

  def __init__(self, channel):
    """Constructor.

    Args:
      channel: A grpc.Channel.
    """
    self.CreateReadRequest = channel.unary_unary(
        '/livestreamreader.MatchReadService/CreateReadRequest',
        request_serializer=livestreamreader_dot_livestreamreader__pb2.ReadRequest.SerializeToString,
        response_deserializer=common_dot_common__pb2.UuidMessage.FromString,
        )
    self.StartReading = channel.unary_unary(
        '/livestreamreader.MatchReadService/StartReading',
        request_serializer=common_dot_common__pb2.UuidMessage.SerializeToString,
        response_deserializer=common_dot_common__pb2.Timestamp.FromString,
        )
    self.StopReading = channel.unary_unary(
        '/livestreamreader.MatchReadService/StopReading',
        request_serializer=common_dot_common__pb2.UuidMessage.SerializeToString,
        response_deserializer=common_dot_common__pb2.EmptyMessage.FromString,
        )
    self.ListReadRequests = channel.unary_unary(
        '/livestreamreader.MatchReadService/ListReadRequests',
        request_serializer=livestreamreader_dot_livestreamreader__pb2.ReadRequestFilter.SerializeToString,
        response_deserializer=livestreamreader_dot_livestreamreader__pb2.ReadRequestArray.FromString,
        )
    self.GetReadRequestByID = channel.unary_unary(
        '/livestreamreader.MatchReadService/GetReadRequestByID',
        request_serializer=common_dot_common__pb2.UuidMessage.SerializeToString,
        response_deserializer=livestreamreader_dot_livestreamreader__pb2.MatchReadRequest.FromString,
        )
    self.DeleteReading = channel.unary_unary(
        '/livestreamreader.MatchReadService/DeleteReading',
        request_serializer=common_dot_common__pb2.UuidMessage.SerializeToString,
        response_deserializer=common_dot_common__pb2.EmptyMessage.FromString,
        )
    self.RecorderStatus = channel.unary_unary(
        '/livestreamreader.MatchReadService/RecorderStatus',
        request_serializer=common_dot_common__pb2.EmptyMessage.SerializeToString,
        response_deserializer=livestreamreader_dot_livestreamreader__pb2.Status.FromString,
        )


class MatchReadServiceServicer(object):
  # missing associated documentation comment in .proto file
  pass

  def CreateReadRequest(self, request, context):
    # missing associated documentation comment in .proto file
    pass
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def StartReading(self, request, context):
    # missing associated documentation comment in .proto file
    pass
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def StopReading(self, request, context):
    # missing associated documentation comment in .proto file
    pass
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def ListReadRequests(self, request, context):
    # missing associated documentation comment in .proto file
    pass
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def GetReadRequestByID(self, request, context):
    # missing associated documentation comment in .proto file
    pass
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def DeleteReading(self, request, context):
    """DeleteReading stops the recorder process.
    When the recorder stops, it is deleted from database.
    """
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def RecorderStatus(self, request, context):
    """RecorderStatus returns the status of the livestream recorder, whether it is
    currently recording a match or not
    """
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')


def add_MatchReadServiceServicer_to_server(servicer, server):
  rpc_method_handlers = {
      'CreateReadRequest': grpc.unary_unary_rpc_method_handler(
          servicer.CreateReadRequest,
          request_deserializer=livestreamreader_dot_livestreamreader__pb2.ReadRequest.FromString,
          response_serializer=common_dot_common__pb2.UuidMessage.SerializeToString,
      ),
      'StartReading': grpc.unary_unary_rpc_method_handler(
          servicer.StartReading,
          request_deserializer=common_dot_common__pb2.UuidMessage.FromString,
          response_serializer=common_dot_common__pb2.Timestamp.SerializeToString,
      ),
      'StopReading': grpc.unary_unary_rpc_method_handler(
          servicer.StopReading,
          request_deserializer=common_dot_common__pb2.UuidMessage.FromString,
          response_serializer=common_dot_common__pb2.EmptyMessage.SerializeToString,
      ),
      'ListReadRequests': grpc.unary_unary_rpc_method_handler(
          servicer.ListReadRequests,
          request_deserializer=livestreamreader_dot_livestreamreader__pb2.ReadRequestFilter.FromString,
          response_serializer=livestreamreader_dot_livestreamreader__pb2.ReadRequestArray.SerializeToString,
      ),
      'GetReadRequestByID': grpc.unary_unary_rpc_method_handler(
          servicer.GetReadRequestByID,
          request_deserializer=common_dot_common__pb2.UuidMessage.FromString,
          response_serializer=livestreamreader_dot_livestreamreader__pb2.MatchReadRequest.SerializeToString,
      ),
      'DeleteReading': grpc.unary_unary_rpc_method_handler(
          servicer.DeleteReading,
          request_deserializer=common_dot_common__pb2.UuidMessage.FromString,
          response_serializer=common_dot_common__pb2.EmptyMessage.SerializeToString,
      ),
      'RecorderStatus': grpc.unary_unary_rpc_method_handler(
          servicer.RecorderStatus,
          request_deserializer=common_dot_common__pb2.EmptyMessage.FromString,
          response_serializer=livestreamreader_dot_livestreamreader__pb2.Status.SerializeToString,
      ),
  }
  generic_handler = grpc.method_handlers_generic_handler(
      'livestreamreader.MatchReadService', rpc_method_handlers)
  server.add_generic_rpc_handlers((generic_handler,))
