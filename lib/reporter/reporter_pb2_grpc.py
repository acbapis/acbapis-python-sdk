# Generated by the gRPC Python protocol compiler plugin. DO NOT EDIT!
import grpc

from reporter import reporter_pb2 as reporter_dot_reporter__pb2


class ReporterServiceStub(object):
  # missing associated documentation comment in .proto file
  pass

  def __init__(self, channel):
    """Constructor.

    Args:
      channel: A grpc.Channel.
    """
    self.ReportJornada = channel.unary_unary(
        '/reporter.ReporterService/ReportJornada',
        request_serializer=reporter_dot_reporter__pb2.ReportJornadaReq.SerializeToString,
        response_deserializer=reporter_dot_reporter__pb2.ReportJornadaResp.FromString,
        )
    self.ClasificacionIndividual = channel.unary_unary(
        '/reporter.ReporterService/ClasificacionIndividual',
        request_serializer=reporter_dot_reporter__pb2.IndividualesRequest.SerializeToString,
        response_deserializer=reporter_dot_reporter__pb2.FileReportResponse.FromString,
        )
    self.ClasificacionesIndividuales = channel.unary_unary(
        '/reporter.ReporterService/ClasificacionesIndividuales',
        request_serializer=reporter_dot_reporter__pb2.IndividualesRequest.SerializeToString,
        response_deserializer=reporter_dot_reporter__pb2.FileReportResponse.FromString,
        )
    self.ResultadoPartido = channel.unary_unary(
        '/reporter.ReporterService/ResultadoPartido',
        request_serializer=reporter_dot_reporter__pb2.BaseRequest.SerializeToString,
        response_deserializer=reporter_dot_reporter__pb2.FileReportResponse.FromString,
        )
    self.MatchBoxscore = channel.unary_unary(
        '/reporter.ReporterService/MatchBoxscore',
        request_serializer=reporter_dot_reporter__pb2.BaseRequest.SerializeToString,
        response_deserializer=reporter_dot_reporter__pb2.FileReportResponse.FromString,
        )
    self.Clasificacion = channel.unary_unary(
        '/reporter.ReporterService/Clasificacion',
        request_serializer=reporter_dot_reporter__pb2.BaseRequest.SerializeToString,
        response_deserializer=reporter_dot_reporter__pb2.FileReportResponse.FromString,
        )
    self.AcumuladosEquipo = channel.unary_unary(
        '/reporter.ReporterService/AcumuladosEquipo',
        request_serializer=reporter_dot_reporter__pb2.AcumuladoEquipoRequest.SerializeToString,
        response_deserializer=reporter_dot_reporter__pb2.FileReportResponse.FromString,
        )
    self.ClasificacionHistorica = channel.unary_unary(
        '/reporter.ReporterService/ClasificacionHistorica',
        request_serializer=reporter_dot_reporter__pb2.IndividualesRequest.SerializeToString,
        response_deserializer=reporter_dot_reporter__pb2.FileReportResponse.FromString,
        )
    self.ClasificacionesHistoricas = channel.unary_unary(
        '/reporter.ReporterService/ClasificacionesHistoricas',
        request_serializer=reporter_dot_reporter__pb2.IndividualesRequest.SerializeToString,
        response_deserializer=reporter_dot_reporter__pb2.FileReportResponse.FromString,
        )
    self.RankingArbitral = channel.unary_unary(
        '/reporter.ReporterService/RankingArbitral',
        request_serializer=reporter_dot_reporter__pb2.BaseRequest.SerializeToString,
        response_deserializer=reporter_dot_reporter__pb2.FileReportResponse.FromString,
        )
    self.StatsEquipos = channel.unary_unary(
        '/reporter.ReporterService/StatsEquipos',
        request_serializer=reporter_dot_reporter__pb2.BaseRequest.SerializeToString,
        response_deserializer=reporter_dot_reporter__pb2.FileReportResponse.FromString,
        )


class ReporterServiceServicer(object):
  # missing associated documentation comment in .proto file
  pass

  def ReportJornada(self, request, context):
    # missing associated documentation comment in .proto file
    pass
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def ClasificacionIndividual(self, request, context):
    # missing associated documentation comment in .proto file
    pass
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def ClasificacionesIndividuales(self, request, context):
    # missing associated documentation comment in .proto file
    pass
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def ResultadoPartido(self, request, context):
    """ResultadoPartido corresponde al fichero XML RESULTADO_PARTIDO
    """
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def MatchBoxscore(self, request, context):
    """MatchBoxscores corresponde al fichero XML PARTIDOS_PLANTILLAS
    """
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def Clasificacion(self, request, context):
    # missing associated documentation comment in .proto file
    pass
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def AcumuladosEquipo(self, request, context):
    # missing associated documentation comment in .proto file
    pass
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def ClasificacionHistorica(self, request, context):
    # missing associated documentation comment in .proto file
    pass
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def ClasificacionesHistoricas(self, request, context):
    # missing associated documentation comment in .proto file
    pass
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def RankingArbitral(self, request, context):
    # missing associated documentation comment in .proto file
    pass
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def StatsEquipos(self, request, context):
    # missing associated documentation comment in .proto file
    pass
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')


def add_ReporterServiceServicer_to_server(servicer, server):
  rpc_method_handlers = {
      'ReportJornada': grpc.unary_unary_rpc_method_handler(
          servicer.ReportJornada,
          request_deserializer=reporter_dot_reporter__pb2.ReportJornadaReq.FromString,
          response_serializer=reporter_dot_reporter__pb2.ReportJornadaResp.SerializeToString,
      ),
      'ClasificacionIndividual': grpc.unary_unary_rpc_method_handler(
          servicer.ClasificacionIndividual,
          request_deserializer=reporter_dot_reporter__pb2.IndividualesRequest.FromString,
          response_serializer=reporter_dot_reporter__pb2.FileReportResponse.SerializeToString,
      ),
      'ClasificacionesIndividuales': grpc.unary_unary_rpc_method_handler(
          servicer.ClasificacionesIndividuales,
          request_deserializer=reporter_dot_reporter__pb2.IndividualesRequest.FromString,
          response_serializer=reporter_dot_reporter__pb2.FileReportResponse.SerializeToString,
      ),
      'ResultadoPartido': grpc.unary_unary_rpc_method_handler(
          servicer.ResultadoPartido,
          request_deserializer=reporter_dot_reporter__pb2.BaseRequest.FromString,
          response_serializer=reporter_dot_reporter__pb2.FileReportResponse.SerializeToString,
      ),
      'MatchBoxscore': grpc.unary_unary_rpc_method_handler(
          servicer.MatchBoxscore,
          request_deserializer=reporter_dot_reporter__pb2.BaseRequest.FromString,
          response_serializer=reporter_dot_reporter__pb2.FileReportResponse.SerializeToString,
      ),
      'Clasificacion': grpc.unary_unary_rpc_method_handler(
          servicer.Clasificacion,
          request_deserializer=reporter_dot_reporter__pb2.BaseRequest.FromString,
          response_serializer=reporter_dot_reporter__pb2.FileReportResponse.SerializeToString,
      ),
      'AcumuladosEquipo': grpc.unary_unary_rpc_method_handler(
          servicer.AcumuladosEquipo,
          request_deserializer=reporter_dot_reporter__pb2.AcumuladoEquipoRequest.FromString,
          response_serializer=reporter_dot_reporter__pb2.FileReportResponse.SerializeToString,
      ),
      'ClasificacionHistorica': grpc.unary_unary_rpc_method_handler(
          servicer.ClasificacionHistorica,
          request_deserializer=reporter_dot_reporter__pb2.IndividualesRequest.FromString,
          response_serializer=reporter_dot_reporter__pb2.FileReportResponse.SerializeToString,
      ),
      'ClasificacionesHistoricas': grpc.unary_unary_rpc_method_handler(
          servicer.ClasificacionesHistoricas,
          request_deserializer=reporter_dot_reporter__pb2.IndividualesRequest.FromString,
          response_serializer=reporter_dot_reporter__pb2.FileReportResponse.SerializeToString,
      ),
      'RankingArbitral': grpc.unary_unary_rpc_method_handler(
          servicer.RankingArbitral,
          request_deserializer=reporter_dot_reporter__pb2.BaseRequest.FromString,
          response_serializer=reporter_dot_reporter__pb2.FileReportResponse.SerializeToString,
      ),
      'StatsEquipos': grpc.unary_unary_rpc_method_handler(
          servicer.StatsEquipos,
          request_deserializer=reporter_dot_reporter__pb2.BaseRequest.FromString,
          response_serializer=reporter_dot_reporter__pb2.FileReportResponse.SerializeToString,
      ),
  }
  generic_handler = grpc.method_handlers_generic_handler(
      'reporter.ReporterService', rpc_method_handlers)
  server.add_generic_rpc_handlers((generic_handler,))
