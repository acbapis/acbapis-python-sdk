# Generated by the gRPC Python protocol compiler plugin. DO NOT EDIT!
import grpc

from acblive import acblive_pb2 as acblive_dot_acblive__pb2
from common import common_pb2 as common_dot_common__pb2
from openapi import openapi_pb2 as openapi_dot_openapi__pb2


class ACBLiveServiceStub(object):
  """ACBLiveService offers methods to query data for live matches.
  If client request a stream, a gRPC stream is opened and kept open
  until the match ends.

  If client request data for a match, the current data is returned.
  """

  def __init__(self, channel):
    """Constructor.

    Args:
      channel: A grpc.Channel.
    """
    self.ListMatches = channel.unary_unary(
        '/acblive.ACBLiveService/ListMatches',
        request_serializer=acblive_dot_acblive__pb2.MatchFilter.SerializeToString,
        response_deserializer=acblive_dot_acblive__pb2.MatchArray.FromString,
        )
    self.GetMatch = channel.unary_unary(
        '/acblive.ACBLiveService/GetMatch',
        request_serializer=common_dot_common__pb2.IdMessage.SerializeToString,
        response_deserializer=openapi_dot_openapi__pb2.MatchDetail.FromString,
        )
    self.StreamMatch = channel.unary_stream(
        '/acblive.ACBLiveService/StreamMatch',
        request_serializer=common_dot_common__pb2.IdMessage.SerializeToString,
        response_deserializer=openapi_dot_openapi__pb2.MatchDetail.FromString,
        )


class ACBLiveServiceServicer(object):
  """ACBLiveService offers methods to query data for live matches.
  If client request a stream, a gRPC stream is opened and kept open
  until the match ends.

  If client request data for a match, the current data is returned.
  """

  def ListMatches(self, request, context):
    # missing associated documentation comment in .proto file
    pass
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def GetMatch(self, request, context):
    # missing associated documentation comment in .proto file
    pass
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def StreamMatch(self, request, context):
    # missing associated documentation comment in .proto file
    pass
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')


def add_ACBLiveServiceServicer_to_server(servicer, server):
  rpc_method_handlers = {
      'ListMatches': grpc.unary_unary_rpc_method_handler(
          servicer.ListMatches,
          request_deserializer=acblive_dot_acblive__pb2.MatchFilter.FromString,
          response_serializer=acblive_dot_acblive__pb2.MatchArray.SerializeToString,
      ),
      'GetMatch': grpc.unary_unary_rpc_method_handler(
          servicer.GetMatch,
          request_deserializer=common_dot_common__pb2.IdMessage.FromString,
          response_serializer=openapi_dot_openapi__pb2.MatchDetail.SerializeToString,
      ),
      'StreamMatch': grpc.unary_stream_rpc_method_handler(
          servicer.StreamMatch,
          request_deserializer=common_dot_common__pb2.IdMessage.FromString,
          response_serializer=openapi_dot_openapi__pb2.MatchDetail.SerializeToString,
      ),
  }
  generic_handler = grpc.method_handlers_generic_handler(
      'acblive.ACBLiveService', rpc_method_handlers)
  server.add_generic_rpc_handlers((generic_handler,))
