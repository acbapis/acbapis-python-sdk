# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: common/common.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()


from google.api import annotations_pb2 as google_dot_api_dot_annotations__pb2


DESCRIPTOR = _descriptor.FileDescriptor(
  name='common/common.proto',
  package='common',
  syntax='proto3',
  serialized_options=_b('Z$bitbucket.org/acbapis/acbapis/common'),
  serialized_pb=_b('\n\x13\x63ommon/common.proto\x12\x06\x63ommon\x1a\x1cgoogle/api/annotations.proto\"\x17\n\tIdMessage\x12\n\n\x02id\x18\x01 \x01(\x03\"\x1b\n\x0bUuidMessage\x12\x0c\n\x04uuid\x18\x01 \x01(\t\"\x1c\n\tTimestamp\x12\x0f\n\x07seconds\x18\x01 \x01(\x03\"\x1b\n\nInt64Array\x12\r\n\x05items\x18\x01 \x03(\x03\"\x0e\n\x0c\x45mptyMessage\"T\n\x07Version\x12\x0f\n\x07Version\x18\x01 \x01(\t\x12\x12\n\nAPIVersion\x18\x02 \x01(\t\x12\x11\n\tGitCommit\x18\x03 \x01(\t\x12\x11\n\tBuildDate\x18\x04 \x01(\tB&Z$bitbucket.org/acbapis/acbapis/commonb\x06proto3')
  ,
  dependencies=[google_dot_api_dot_annotations__pb2.DESCRIPTOR,])




_IDMESSAGE = _descriptor.Descriptor(
  name='IdMessage',
  full_name='common.IdMessage',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='id', full_name='common.IdMessage.id', index=0,
      number=1, type=3, cpp_type=2, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=61,
  serialized_end=84,
)


_UUIDMESSAGE = _descriptor.Descriptor(
  name='UuidMessage',
  full_name='common.UuidMessage',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='uuid', full_name='common.UuidMessage.uuid', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=86,
  serialized_end=113,
)


_TIMESTAMP = _descriptor.Descriptor(
  name='Timestamp',
  full_name='common.Timestamp',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='seconds', full_name='common.Timestamp.seconds', index=0,
      number=1, type=3, cpp_type=2, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=115,
  serialized_end=143,
)


_INT64ARRAY = _descriptor.Descriptor(
  name='Int64Array',
  full_name='common.Int64Array',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='items', full_name='common.Int64Array.items', index=0,
      number=1, type=3, cpp_type=2, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=145,
  serialized_end=172,
)


_EMPTYMESSAGE = _descriptor.Descriptor(
  name='EmptyMessage',
  full_name='common.EmptyMessage',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=174,
  serialized_end=188,
)


_VERSION = _descriptor.Descriptor(
  name='Version',
  full_name='common.Version',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='Version', full_name='common.Version.Version', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='APIVersion', full_name='common.Version.APIVersion', index=1,
      number=2, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='GitCommit', full_name='common.Version.GitCommit', index=2,
      number=3, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='BuildDate', full_name='common.Version.BuildDate', index=3,
      number=4, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=190,
  serialized_end=274,
)

DESCRIPTOR.message_types_by_name['IdMessage'] = _IDMESSAGE
DESCRIPTOR.message_types_by_name['UuidMessage'] = _UUIDMESSAGE
DESCRIPTOR.message_types_by_name['Timestamp'] = _TIMESTAMP
DESCRIPTOR.message_types_by_name['Int64Array'] = _INT64ARRAY
DESCRIPTOR.message_types_by_name['EmptyMessage'] = _EMPTYMESSAGE
DESCRIPTOR.message_types_by_name['Version'] = _VERSION
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

IdMessage = _reflection.GeneratedProtocolMessageType('IdMessage', (_message.Message,), dict(
  DESCRIPTOR = _IDMESSAGE,
  __module__ = 'common.common_pb2'
  # @@protoc_insertion_point(class_scope:common.IdMessage)
  ))
_sym_db.RegisterMessage(IdMessage)

UuidMessage = _reflection.GeneratedProtocolMessageType('UuidMessage', (_message.Message,), dict(
  DESCRIPTOR = _UUIDMESSAGE,
  __module__ = 'common.common_pb2'
  # @@protoc_insertion_point(class_scope:common.UuidMessage)
  ))
_sym_db.RegisterMessage(UuidMessage)

Timestamp = _reflection.GeneratedProtocolMessageType('Timestamp', (_message.Message,), dict(
  DESCRIPTOR = _TIMESTAMP,
  __module__ = 'common.common_pb2'
  # @@protoc_insertion_point(class_scope:common.Timestamp)
  ))
_sym_db.RegisterMessage(Timestamp)

Int64Array = _reflection.GeneratedProtocolMessageType('Int64Array', (_message.Message,), dict(
  DESCRIPTOR = _INT64ARRAY,
  __module__ = 'common.common_pb2'
  # @@protoc_insertion_point(class_scope:common.Int64Array)
  ))
_sym_db.RegisterMessage(Int64Array)

EmptyMessage = _reflection.GeneratedProtocolMessageType('EmptyMessage', (_message.Message,), dict(
  DESCRIPTOR = _EMPTYMESSAGE,
  __module__ = 'common.common_pb2'
  # @@protoc_insertion_point(class_scope:common.EmptyMessage)
  ))
_sym_db.RegisterMessage(EmptyMessage)

Version = _reflection.GeneratedProtocolMessageType('Version', (_message.Message,), dict(
  DESCRIPTOR = _VERSION,
  __module__ = 'common.common_pb2'
  # @@protoc_insertion_point(class_scope:common.Version)
  ))
_sym_db.RegisterMessage(Version)


DESCRIPTOR._options = None
# @@protoc_insertion_point(module_scope)
